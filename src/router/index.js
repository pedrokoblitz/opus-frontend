import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainView',
    component: () => import('../screens/MainView.vue')
  },
  {
    path: '/apps/:action?/:id?',
    name: 'AppsView',
    component: () => import('../screens/AppsView.vue')
  },
  {
    path: '/users/:action?/:id?',
    name: 'UsersView',
    component: () => import('../screens/UsersView.vue')
  },
  {
    path: '/types/:action?/:id?',
    name: 'TypesView',
    component: () => import('../screens/TypesView.vue')
  },
  {
    path: '/roles/:action?/:id?',
    name: 'RolesView',
    component: () => import('../screens/RolesView.vue')
  },
  {
    path: '/confirm',
    name: 'ConfirmationView',
    component: () => import('../screens/ConfirmationView.vue')
  },
  {
    path: '/notification',
    name: 'NotificationView',
    component: () => import('../screens/NotificationView.vue')
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
