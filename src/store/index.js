import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {indexBy} from "../helpers.js"

Vue.use(Vuex);

const client = axios.create({baseURL:"http://localhost:8000/api"});

const store = new Vuex.Store({
  state: {
    apps: {},
    roles: {},
    types: {},
    users: {},
    confirmation: {},
    notifications: [],
    loading: false,
  },

  getters: {},  
  
  mutations: {
    /*
     *
     */ 
    init(state, data) {
      state.apps = {};
      data.apps.forEach(i => {
        state.apps[i.id] = i;
      });

      state.roles = {};
      data.roles.forEach(i => {
        state.roles[i.id] = i;
      });

      state.types = {};
      data.types.forEach(i => {
        state.types[i.id] = i;
      });

      state.users = {};
      data.users.forEach(i => {
        state.users[i.id] = i;
      });

      state.notifications = [];
      state.confirmation = [];
      state.app = {};
    },

    /*
     *
     */ 
    notify(state, notification) {
      state.notifications.push(notification);
    },

    /*
     *
     */ 
    setConfirmation(state, confirmation) {
      state.confirmation = confirmation;
    },

    /*
     *
     */ 
    clearNotifications(state) {
      state.notifications = [];
    },

    /*
     *
     */ 
    setLoading(state) {
      state.loading = true;
    },

    /*
     *
     */ 
    setReady(state) {
      state.loading = false;
    },

    /*
     *
     */ 
    clearConfirmation(state) {
      state.confirmation = {};
    },

    /*
     *
     */ 
    load(state, entity, items) {
      state[entity] = indexBy(items);
    },

    /*
     *
     */ 
    update(state, entity, item) {
      state[entity][item.id] = item;
    },

    /*
     *
     */ 
    remove(state, entity, id) {
      delete state[entity][id];
    },

    /*
     *
     */ 
    removePermission(state, permission) {
      for (const key in state.roles) {
        if (Object.hasOwnProperty.call(state.roles, key)) {
          const role = state.roles[key];
          state.roles[role.id].permissions = role.permissions.filter(i => { return i.id != permission.id});
        }
      }
    },

    /*
     *
     */ 
    removeProperty(state, property) {
      for (const key in state.types) {
        if (Object.hasOwnProperty.call(state.types, key)) {
          const type = state.types[key];
          state.types[type.id].properties = type.properties.filter(i => { return i.id != property.id});
        }
      }
    },

    /*
     *
     */ 
    removeStatus(state, status) {
      for (const key in state.types) {
        if (Object.hasOwnProperty.call(state.types, key)) {
          const type = state.types[key];
          state.types[type.id].statuses = type.statuses.filter(i => { return i.id != status.id});
        }
      }
    },

    /*
     *
     */ 
    removeParent(state, parent) {
      for (const key in state.types) {
        if (Object.hasOwnProperty.call(state.types, key)) {
          const type = state.types[key];
          state.types[type.id].allowedParents = type.allowedParents.filter(i => { return i.id != parent.id});
        }
      }
    },

    /*
     *
     */ 
    removeRelationship(state, relationship) {
      for (const key in state.types) {
        if (Object.hasOwnProperty.call(state.types, key)) {
          const type = state.types[key];
          state.types[type.id].relationship = type.relationship.filter(i => { return i.id != relationship.id});
        }
      }
    },
  },

  actions : {
    /*
     *
     */ 
    init({commit}) {
      commit('clearNotifications');
      commit('setLoading');
      return client.get('/admin')
        .then(response => {
          const items = response.data.data;
          commit('init', items);
          commit('notify', {notification:'loading', status:'success'});
          commit('setReady');
        }).catch(error => {
          commit('notify', {notification: error, status: 'danger'});
          commit('setReady');
          // console.log(error);
        });
    },
      
    /*
     *
     */ 
    load({commit}, payload) {
      commit('clearNotifications');
      commit('setLoading');
      return client.get('/' + payload.entity )
        .then(response => {
          const items = response.data.data;
          commit('load', payload.entity, items);
          commit('notify', {notification: 'loaded ' + payload.entity, status: 'success'});
          commit('setReady');
        }).catch(error => {
          commit('notify', {notification: error, status: 'danger'});
          commit('setReady');
        });
    },

    /*
     *
     */ 
    save({commit}, payload) {
      commit('clearNotifications');
      commit('setLoading');

      let url, method;
      url = "/node/";
      if (payload.id) {
        url += payload.entity + '/' + payload.id;
        method = 'put';
      }
      if (!payload.id) {
        url += payload.entity;
        method = 'post';
      }

      return client[method](url, payload)
        .then(response => {
          const item = response.data.data;
          commit('update', payload.entity, item);
          commit('notify', {notification: 'saved ' + payload.entity, status: 'success'});
          commit('setReady');
          return response;
        })
        .catch(error => {
          commit('notify', {notification: error, status: 'danger'});
          commit('setReady');
        });
    },

    /*
     *
     */ 
    delete({commit}, payload) {
      commit('clearNotifications');
      commit('setLoading');
      return client.delete('/' + payload.entity + '/' + payload.id)
        .then(() => {
          commit('delete', payload.entity, payload.id);
          commit('notify', {notification: 'deleted ' + payload.entity, status: 'success'});
          commit('setReady');
        }).catch(error => {
          commit('notify', {notification: error, status: 'danger'});
          commit('setReady');
        });
    },
  
  }
});

export default store;
