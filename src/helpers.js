function toIdsArray(input) {
    return input.map((i) => { return i.id});
}

function toArray(object) {
    return Object.values(object);
}

function indexBy(items, key = 'id') {
    let result = {};
    for (let i = 0; i < items.length; i++) {
      const index = items[i][key];
      result[index] = items[i];
    }
    return result;
}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
